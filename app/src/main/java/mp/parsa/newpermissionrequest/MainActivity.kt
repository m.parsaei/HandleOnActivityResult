package mp.parsa.newpermissionrequest

import android.Manifest
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object{
        const val TAG = "MainActivity"
    }

    val permissionRequester =
        prepareCall(ActivityResultContracts.RequestPermission()) { permissionGranted: Boolean ->
            if (permissionGranted)
                Toast.makeText(this, "Permission has been accepted", Toast.LENGTH_SHORT).show()
            else
                Toast.makeText(this, "Permission has been denied", Toast.LENGTH_SHORT).show()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnGetPermission.setOnClickListener {
            permissionRequester.launch(Manifest.permission.ACCESS_FINE_LOCATION)
        }
    }
}
